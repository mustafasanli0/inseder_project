(function () {
    var CSS = {
        arena: {
            width: 720,
            height: 600,
            background: '#62247B',
            position: 'fixed',
            top: '50%',
            left: '50%',
            zIndex: '999',
            transform: 'translate(-50%, -50%)'
        },
        balls: [],
        line: {
            width: 0,
            height: 600,
            borderLeft: '2px dashed #C6A62F',
            position: 'absolute',
            top: 0,
            left: '50%'
        },
        stick: {
            width: 12,
            height: 85,
            position: 'absolute',
            background: '#C6A62F'
        },
        stick1: {
            left: 0,
            top: 150
        },
        stick2: {
            left: () => (CSS.arena.width-CSS.stick.width), // ARROW FUNCTION
            top: 150
        },
        scoreBoard: {
            width: 190,
            height: 30,
            position: 'absolute',
            left:'40%',
            color:'red',
            fontSize:'20px'
        },
    };

    var CONSTS = {
    	gameSpeed: 20,
        score1: 0,
        score2: 0,
        stick1Speed: 0,
        stick2Speed: 0,
        ballsOptions: [],
        winnerScore: 5
    };

    function start() {
      
        
        draw();
        createBall({ 
            width: 15,
            height: 15,
            position: 'absolute',
            top: 0,
            left: 350,
            borderRadius: 50,
            background: '#C6A62F'
        },{
            ballTopSpeed: 0,
            ballLeftSpeed: 0
        });
        if(!getScores())
            setScores({'score1':0,'score2':0});

        getScoreBoard();
        setEvents();
      
       
    }

    function createBall(ball,ballOptions){
        CONSTS.ballsOptions.push(ballOptions);
        CSS.balls.push(ball);
        $('<div/>', {id: `pong-ball${CSS.balls.length-1}`}).css(CSS.balls[CSS.balls.length-1]).appendTo('#pong-game');

        roll(0,CSS.balls.length-1);
        loop(CSS.balls.length-1);
    }

    function draw(i) { //i=Ball id
        $('<div/>', {id: 'pong-game'}).css(CSS.arena).appendTo('body');
        $('<div/>', {id: 'scoreBoard'}).css(CSS.scoreBoard).appendTo('#pong-game');
        $('<div/>', {id: 'pong-line'}).css(CSS.line).appendTo('#pong-game');
        $('<div/>', {id: 'stick-1'}).css($.extend(CSS.stick1, CSS.stick)).appendTo('#pong-game');
        $('<div/>', {id: 'stick-2'}).css($.extend(CSS.stick2, CSS.stick)).appendTo('#pong-game');
        
    }

    function setEvents() {
        $(document).on('keydown', function (e) {
           
            if (e.keyCode == 87) {
                CONSTS.stick1Speed = -6;
            }
            else if (e.keyCode == 83 ){
                CONSTS.stick1Speed = 6;
            }
            if (e.keyCode == 38 ){
                CONSTS.stick2Speed = -6;
            }
            else if (e.keyCode == 40 ){
                CONSTS.stick2Speed = 6;
            }

        });
        

        $(document).on('keyup', function (e) {
            if (e.keyCode == 87) {
                CONSTS.stick1Speed = 0;
            }
            else if (e.keyCode == 83 ){
                CONSTS.stick1Speed = 0;
            }
            if (e.keyCode == 40 ){
                CONSTS.stick2Speed = 0;
            }
            else if (e.keyCode == 38 ){
                CONSTS.stick2Speed = 0;
            }
        });
        $(document).on('click',function(e){
            createBall({ 
                width: 15,
                height: 15,
                position: 'absolute',
                top: 0,
                left: 350,
                borderRadius: 50,
                background: '#C6A62F'
            },{
                ballTopSpeed: 0,
                ballLeftSpeed: 0
            });
        })
    }

    function loop(i) { // i = Ball İd
        window.pongLoop = setInterval(function () {
            CSS.stick1.top += CONSTS.stick1Speed;
            $('#stick-1').css('top', CSS.stick1.top);
            
            CSS.stick2.top += CONSTS.stick2Speed;
            $('#stick-2').css('top', CSS.stick2.top);
                       
            CSS.balls[i].top += CONSTS.ballsOptions[i].ballTopSpeed;
            
            CSS.balls[i].left += CONSTS.ballsOptions[i].ballLeftSpeed;

            if (CSS.balls[i].top <= 0 ||
                CSS.balls[i].top >= CSS.arena.height - CSS.balls[i].height) {
                CONSTS.ballsOptions[i].ballTopSpeed = CONSTS.ballsOptions[i].ballTopSpeed * -1;
            }
            
            $(`#pong-ball${i}`).css({top: CSS.balls[i].top,left: CSS.balls[i].left});


            if (CSS.balls[i].left <= CSS.stick.width) {
                CSS.balls[i].top > CSS.stick1.top && CSS.balls[i].top < CSS.stick1.top + CSS.stick.height && (CONSTS.ballsOptions[i].ballLeftSpeed = CONSTS.ballsOptions[i].ballLeftSpeed * -1) || roll(2,i); // goalPlayer
            }
            else if (CSS.balls[i].left >= CSS.arena.width - CSS.balls[i].width - CSS.stick.width) {
                CSS.balls[i].top > CSS.stick2.top && CSS.balls[i].top < CSS.stick2.top + CSS.stick.height && (CONSTS.ballsOptions[i].ballLeftSpeed = CONSTS.ballsOptions[i].ballLeftSpeed * -1) || roll(1,i); // goalPlayer
            }



            
            

        }, CONSTS.gameSpeed);
    }
    
    /**
     * 
     * @return object
     */
    function getScores(){
        return JSON.parse(localStorage.getItem('scores'));
    }

    function setScores(scores){
        localStorage.setItem('scores',JSON.stringify(scores));
    }

    function getScoreBoard(){
        $('#scoreBoard').html(`<span style='margin-right:80px;'>P1: ${getScores()['score1']}</span><span>P2: ${getScores()['score2']}</span>`);
    }

    function increaseScore(goalPlayer){
        var scoreJson = getScores();
        scoreJson['score'+goalPlayer]++
        localStorage.setItem('scores',JSON.stringify(scoreJson));
        checkWinner(); // if score 5 alert winner user
        getScoreBoard();
    }

    function checkWinner(){

        var scores = getScores();

        if(scores['score1'] == CONSTS.winnerScore){
            alert("Winner P1");
            setScores({'score1':0,'score2':0});
        }
        else if(scores['score2'] == CONSTS.winnerScore){
            alert("Winner P2");
            setScores({'score1':0,'score2':0});
        }

    }

   

    

    function roll(goalPlayer=0,i=0) {
            
        if(goalPlayer == 1 || goalPlayer == 2) //spesific player
            increaseScore(goalPlayer); // increase player totalgoal

        CSS.balls[i].top = 250;
        CSS.balls[i].left = 350;
        var side = -1;

        if (Math.random() < 0.5) {
            side = 1;
        }

        CONSTS.ballsOptions[i].ballTopSpeed = Math.random() * -2 - 3;
        CONSTS.ballsOptions[i].ballLeftSpeed = side * (Math.random() * 2 + 3);
    }


    function cpuPlayer1(){

        setInterval(function(){

        if(CSS.balls[0].top > CSS.stick1.top )
            CONSTS.stick1Speed = 10
        else if(CSS.balls[0].top < CSS.stick1.top )
            CONSTS.stick1Speed = -10
        
        }, CONSTS.gameSpeed*(Math.random() * 10 + 1)); // RANDOM CHANCE CONST
    }

    function cpuPlayer2(){

        setInterval(function(){

        if(CSS.balls[0].top > CSS.stick2.top )
            CONSTS.stick2Speed = 10
        else if(CSS.balls[0].top < CSS.stick2.top )
            CONSTS.stick2Speed = -10
        }, CONSTS.gameSpeed*(Math.random() * 10 + 1)); // RANDOM CHANCE CONST
    }

    

    start();
    // cpuPlayer1(); // DEFAULT DISABLE
    // cpuPlayer2(); // DEFAULT DISABLE

   

})();
